alias 'l=exa -bghl --sort created --git'
alias 'la=l -a'

sl () { streamlink -p mpv --player-passthrough=http "$@" best }
